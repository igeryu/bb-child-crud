package com.magipixel.bountyboard.child.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface ChildRepository extends CrudRepository<Child, Long> {
    @PreAuthorize("hasRole('ADMIN')")
    @Override
    void deleteById(final Long id);

    @PreAuthorize("hasRole('PARENT')")
    @Override
    Iterable<Child> findAll();

    @PreAuthorize("hasRole('PARENT')")
    @Override
    Optional<Child> findById(final Long id);

    List<Child> findByIdIn(@RequestParam("ids") Long[] ids);

    @PreAuthorize("hasRole('PARENT')")
    @Override
    <S extends Child> S save(final S child);
}