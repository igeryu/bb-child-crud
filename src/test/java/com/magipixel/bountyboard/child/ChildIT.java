package com.magipixel.bountyboard.child;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.magipixel.bountyboard.child.model.Child;
import com.magipixel.bountyboard.child.model.ChildRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@Tag("integration")
class ChildIT {

    private static int idCount = 0;

    @BeforeAll
    static void resetIdCount() {
        ChildIT.idCount = 0;
    }

    @Nested
    @DisplayName("Mapping")
    @AutoConfigureMockMvc
    @SpringBootTest
    class Mapping {

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private ChildRepository repository;

        @Test
        @WithMockUser(roles = "PARENT")
        void findByIdIn_oneChild_shouldCallCorrectMethod() throws Exception {
            // Arrange
            final long id = repository.save(new Child()).getId();

            // Act Assert
            mockMvc.perform(get("/children/search/findByIdIn")
                    .param("ids", String.valueOf(id))
                    .accept("application/json"))
                    .andExpect(status()
                            .isOk());
        }

        @Test
        @WithMockUser(roles = "PARENT")
        void findByIdIn_twoChildren_singleList_shouldCallCorrectMethod() throws Exception {
            // Arrange
            final long idA = repository.save(new Child()).getId();
            final long idB = repository.save(new Child()).getId();
            assertThat(idA, is(not(equalTo(idB))));

            // Act / Assert
            mockMvc.perform(get("/children/search/findByIdIn?ids=" + idA + "," + idB)
                    .accept("application/json"))
                    .andExpect(status()
                            .isOk());
        }

        @Test
        @WithMockUser(roles = "PARENT")
        void findByIdIn_twoChildren_multiParam_shouldCallCorrectMethod() throws Exception {
            // Arrange
            final long idA = repository.save(new Child()).getId();
            final long idB = repository.save(new Child()).getId();
            assertThat(idA, is(not(equalTo(idB))));

            // Act / Assert
            mockMvc.perform(get("/children/search/findByIdIn")
                    .param("ids", String.valueOf(idA))
                    .param("ids", String.valueOf(idB))
                    .accept("application/json"))
                    .andExpect(status()
                            .isOk());
        }
    }

    @Nested
    @DisplayName("Security")
    @AutoConfigureMockMvc
    @SpringBootTest
    class Security {

        @Autowired
        private MockMvc mockMvc;

        @BeforeEach
        void addChild() throws Exception {
            mockMvc.perform(post("/children")
                    .with(user("username")
                            .roles("PARENT"))
                    .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"username\": \"can_be_anything\"}"))
                    .andExpect(status()
                            .isCreated());
            ChildIT.idCount++;
        }

        @Test
        void shouldReturn401WhenNoTokenSent() throws Exception {
            mockMvc.perform(get("/children")
                    .accept("application/json"))
                    .andExpect(status()
                            .isUnauthorized());
        }

        @Test
        @WithMockUser(roles = "CHILD")
        void getProjects_shouldThrowErrorIfInvalidRole() throws Exception {
            // Act / Assert
            mockMvc.perform(get("/children")
                    .accept("application/json"))
                    .andExpect(status()
                            .isForbidden());
        }

        @Test
        @WithMockUser(roles = "PARENT")
        void getProjects_shouldReturnOkayIfParent() throws Exception {
            mockMvc.perform(get("/children")
                    .accept("application/json"))
                    .andExpect(status()
                            .isOk());
        }

        @Test
        @WithMockUser(roles = "CHILD")
        void findById_shouldThrowErrorIfInvalidRole() throws Exception {
            // Act / Assert
            mockMvc.perform(get("/children/1")
                    .accept("application/json"))
                    .andExpect(status()
                            .isForbidden());
        }

        @Test
        @WithMockUser(roles = "PARENT")
        void findById_shouldReturnOkayIfParent() throws Exception {
            mockMvc.perform(get("/children/" + ChildIT.idCount)
                    .accept("application/json"))
                    .andExpect(status()
                            .isOk());
        }

        @Test
        @WithMockUser(roles = "CHILD")
        void createChild_shouldThrowErrorIfInvalidRole() throws Exception {
            // Act / Assert
            mockMvc.perform(post("/children")
                    .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"username\": \"can_be_anything\"}"))
                    .andExpect(status()
                            .isForbidden());
        }

        @Test
        @WithMockUser(roles = "PARENT")
        void createChild_shouldReturnOkayIfParent() throws Exception {
            // Act / Assert
            mockMvc.perform(post("/children")
                    .accept("application/json")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"name\": \"can_be_anything\"}"))
                    .andExpect(status()
                            .isCreated());
        }

        @Test
        @WithMockUser(roles = {"PARENT", "CHILD"})
        void deleteById_shouldThrowErrorForEveryone() throws Exception {
            // Act / Assert
            mockMvc.perform(delete("/children/" + ChildIT.idCount)
                    .accept("application/json"))
                    .andExpect(status()
                            .isForbidden());
        }
    }

    @Nested
    @DisplayName("Status Codes")
    @AutoConfigureMockMvc
    @SpringBootTest
    class StatusCodes {

        @Autowired
        private MockMvc mockMvc;

        @Autowired
        private ObjectMapper mapper;

        @Test
        @DisplayName("Should return 409 Conflict, when duplicate Child created")
        @WithMockUser(roles = "PARENT")
        void shouldReturn_409Conflict_whenDuplicateAccountCreated() throws Exception {
            // Arrange
            final String duplicatedEmail = "xyz@abc.com";
            final String child = "{\"email\": \"" + duplicatedEmail + "\"}";
            mockMvc.perform(post("/children")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(child)
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status()
                            .isCreated());

            // Act / Assert
            final String resultAsString = mockMvc.perform(post("/children")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(child)
                    .accept(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status()
                            .isConflict())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();

            assertThat("Expected response object", resultAsString, not(isEmptyString()));
            final Map<String, Object> resultAsMap = mapper.readValue(resultAsString, new TypeReference<Map<String, Object>>() {
            });
            assertThat("Expected 'cause' field to not exist", resultAsMap.get("cause"), nullValue());
            assertThat("Expected 'message' field to exist", resultAsMap.get("message"), notNullValue());
            final String expectedMessage = "Child already exists";
            assertThat("Expected 'message' field to have correct message", resultAsMap.get("message"), equalTo(expectedMessage));

        }

    }
}
