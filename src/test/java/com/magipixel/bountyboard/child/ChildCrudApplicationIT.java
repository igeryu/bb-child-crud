package com.magipixel.bountyboard.child;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.number.OrderingComparison.greaterThan;

@SpringBootTest
@RequiredArgsConstructor
@Tag("integration")
class ChildCrudApplicationIT {
    private final ApplicationContext context;

    @Test
    void contextLoads() {
        assertThat(context.getBeanDefinitionCount(), is(greaterThan(0)));
    }

}