package com.magipixel.bountyboard.child.model;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.number.OrderingComparison.greaterThan;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Tag("unit")
class ChildRepositoryTest {

    @Autowired
    private ChildRepository repository;

    @Test
    @WithMockUser(roles = "PARENT")
    void findByIdIn() {
        // Arrange
        //    Add two Child entities to search for
        final Child childA = new Child();
        childA.setEmail("A");
        final Long idA = repository.save(childA).getId();
        final Child childB = new Child();
        childB.setEmail("B");
        final Long idB = repository.save(childB).getId();

        //    Add distraction Child
        final Child childC = new Child();
        childC.setEmail("C");
        repository.save(childC);

        // Act
        final List<Child> result = repository.findByIdIn(new Long[]{idA, idB});

        // Assert
        assertThat("Repository returned null", result, notNullValue());
        assertThat("Repository returned empty list", result, hasSize(greaterThan(0)));
        assertThat("Repository returned list with only one element", result, hasSize(greaterThan(1)));
        assertThat("Repository returned list with more than two elements", result, hasSize(2));
        childA.setId(idA);
        childB.setId(idB);
        assertThat("Repository returned list with more than two elements", result, hasItems(childA, childB));

    }

}